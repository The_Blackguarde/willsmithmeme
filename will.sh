#!/usr/bin/env sh
#echo $@
#echo $#

youtube-dl -o vid.mp4 -f mp4 $1 > /dev/null

ffmpeg -ss $2 -i vid.mp4 -t 00:00:02.40 cutvideo.mp4 

ffmpeg -i cutvideo.mp4 -i frame2.png -filter_complex "[1][0]scale2ref[i][m];[m][i]overlay[v]" -map "[v]" -map 0:a? overlay.mp4 

ffmpeg -i overlay.mp4 -vn overlay.aac 

ffmpeg -i will2.aac -i overlay.aac -filter_complex amerge audiofinal.aac

ffmpeg -i overlay.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate2.ts

ffmpeg -i "concat:intermediate1.ts|intermediate2.ts" -c copy -bsf:a aac_adtstoasc output.mp4

ffmpeg -i output.mp4 -an outputnoaudio.mp4

ffmpeg -i "concat:will1.aac|audiofinal.aac" -c copy result.aac

ffmpeg -i outputnoaudio.mp4 -i result.aac result.mp4 

date1=$(date +%d-%m-%Y" "%H:%M:%S)
date2="Will Smith That's hot "
date=$date2$date1
youtube-upload --title="$date" --playlist="Will Smith Memes" --client-secrets=".client_secrets.json" --credentials-file=".youtube-upload-credentials.json" result.mp4


