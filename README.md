# Make Will Smith rewind 2018 meme video's 

A bash scrip that takes a youtube video and a timestamp and combines it with Will Smith. 

## Getting started 

Follow the instructions on the youtube_upload github to get credential files for your youtube channel. Name it .client_secrets.json. Run the youtube-upload command to generate the credential file, name it .youtube-upload-credentials.json. After this you should be able to run the script. 

### Prerequisites 

You need Docker or python3.7, ffmpeg, youtube_dl, google-api-python-client, oauth2client, progressbar2, youtube-upload

### Usage

docker run --rm will < youtubelink > < timestamp >

example

docker run --rm will https://www.youtube.com/watch?v=hdJzjttE0wI 00:00:03

### TODO

Documentation

Improve code

Only return 1 link
