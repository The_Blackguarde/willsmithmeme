FROM python:3.7-alpine
RUN apk add  --no-cache ffmpeg
RUN pip install youtube_dl
RUN pip install google-api-python-client oauth2client progressbar2
RUN wget https://github.com/tokland/youtube-upload/archive/master.zip
RUN unzip master.zip
WORKDIR youtube-upload-master
COPY main.py youtube_upload/
RUN python setup.py install 

COPY . /youtube

WORKDIR /youtube

ENTRYPOINT ["/bin/sh", "will.sh"]




